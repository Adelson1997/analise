package com.analise.credito.controller;

import java.math.BigDecimal;
import java.util.List;

import org.apache.logging.log4j.status.StatusLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analise.credito.model.Pessoa;

import com.analise.credito.repository.PessoaRepository;

@RestController
@RequestMapping(value="/api")
public class PessoaController {

	//Injeção de dependência do repositório 
	@Autowired
	PessoaRepository pr;
	
	
	//Este metodo lista as pessoas 
	@GetMapping(value="/pessoas")
	public List<Pessoa> listaPessoas(){
		
		return pr.findAll();
	}
	
	//Retorna uma pessoa por CPF
	@GetMapping(value="/pessoa/{cpf}")
	public Pessoa retornaPessoaPorCpf(@PathVariable String cpf) {
		return pr.findPessoaByCpf(cpf) ;
		
	}
	
	
	//Este metodo salva uma pessoa *não é necessário enviar no corpo da requisição um ID pois o mesmo é gerado automaticamente*
	@PostMapping(value="/pessoa")
	public Pessoa salvaPessoa(@RequestBody Pessoa pessoa) {
		if(pessoa.getCpf().length() > 8) {
			
			pessoa.setCpf("CPF inválido, contém mais de 8 caracteres"); 
		}
		if(pessoa.getScore() < 400) {
			pessoa.setResultado(0);
			
		}
		if(pessoa.getScore() >= 400 ) {
			pessoa.setResultado(500);
			
		}
		return pr.save(pessoa);
	}
	
	
	/*metodo no qual atualiza dados de uma pessoa *caso entre com um CPF que contenha mais de 8 caracteres esse metodo deverá ser utilizado para corrigir*
	 * Neste método é necessário enviar o ID que queremos atualizar no corpo da requisição*/
	@PutMapping(value="/pessoa")
	public Pessoa alteraPessoa(@RequestBody Pessoa pessoa) {
		
		
		return pr.save(pessoa);
		
	}
	
}
