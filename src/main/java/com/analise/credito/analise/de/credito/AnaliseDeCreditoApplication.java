package com.analise.credito.analise.de.credito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan("com.analise.credito.controller")
@EntityScan("com.analise.credito.model")
@EnableJpaRepositories("com.analise.credito.repository")
public class AnaliseDeCreditoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnaliseDeCreditoApplication.class, args);
	}

}
