# A Entrada de dados
##### A Entrada de dados é feita com nome, cpf, idade, dependentes e Score (que e necessária uma outra API para defini-lo) e temos o resultado, que necessita de uma formula de calculo entre os dados anteriores.

# Metodos

##### Para a entrada de dados ser concluída é preciso informar os dados corretos, por exemplo no objeto CPF que não pode conter mais de 8 caracteres pois quando o mesmo for analisado pela outra API  não ocorra um erro


# Instruções

##### Esse pequeno modelo foi criado porém apenas receb poucos dados, é necessário mais metodos e mais informações para a conclusão do projeto como por exemplo a formula para calcular o score ou a API na qual virá o score
##### Então levando em consideração a média de scores utilizados por outras empresas eu decidi colocar valores ficticios onde o resultado será calculado com base neste valor
